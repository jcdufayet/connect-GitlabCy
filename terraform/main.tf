resource "null_resource" "virtual_machine" {
  triggers = {
    current_time = timestamp()
  }

  provisioner "local-exec" {
    command = "./install_security_tool.sh"
  }
}
