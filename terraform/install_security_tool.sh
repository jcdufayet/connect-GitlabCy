# Script d'installation/mise à jour de l'outil de sécurité (nmap dans cet exemple)

# Vérifier si l'outil de sécurité est déjà présent
if command -v nmap &> /dev/null; then
    # Vérifier la version actuelle de nmap
    current_version=$(nmap --version | head -n 1 | awk '{print $3}')
    echo "L'outil de sécurité (nmap) est déjà installé en version $current_version."

    # Vérifier la version la plus récente disponible via Homebrew
    if command -v brew &> /dev/null; then
        latest_version=$(brew info nmap | awk '/Version/ {print $NF}')
        if [ "$current_version" == "$latest_version" ]; then
            echo "L'outil de sécurité (nmap) est déjà à la version la plus récente."
        else
            echo "Mise à jour de l'outil de sécurité (nmap) vers la version $latest_version."
            brew upgrade nmap
        fi
    else
        echo "Homebrew n'est pas installé. Veuillez installer Homebrew manuellement : https://brew.sh/"
        exit 1
    fi
else
    # Installer l'outil de sécurité (nmap dans cet exemple)
    if command -v brew &> /dev/null; then
        echo "Installation de l'outil de sécurité (nmap)..."
        brew install nmap
    else
        echo "Homebrew n'est pas installé. Veuillez installer Homebrew manuellement : https://brew.sh/"
        exit 1
    fi
fi